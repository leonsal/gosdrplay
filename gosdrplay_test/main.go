package main

import (
	"fmt"

	sdr "bitbucket.org/leonsal/gosdrplay"
)

func main() {

	// Enable emission of debug messages
	err := sdr.DebugEnable(true)
	if err != nil {
		panic(err)
	}

	// Get and show API version
	ver, err := sdr.ApiVersion()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Api Version:%v\n", ver)

	// Enable / disable DC offset and IQ imbalance control
	err = sdr.DCoffsetIQimbalanceControl(false, false)
	if err != nil {
		panic(err)
	}
	err = sdr.DCoffsetIQimbalanceControl(false, true)
	if err != nil {
		panic(err)
	}
	err = sdr.DCoffsetIQimbalanceControl(true, false)
	if err != nil {
		panic(err)
	}
}
