// package sdrplay implement a Go binding to the mirsdrapi-rsp C library.

package sdrplay

// // Include files needed by the Go binding
// #include <stdlib.h>
// #include "mirsdrapi-rsp.h"
//
import "C"

const (
	Success            = C.mir_sdr_Success
	Fail               = C.mir_sdr_Fail
	InvalidParam       = C.mir_sdr_InvalidParam
	OutOfRange         = C.mir_sdr_OutOfRange
	GainUpdateError    = C.mir_sdr_GainUpdateError
	RfUpdateError      = C.mir_sdr_RfUpdateError
	FsUpdateError      = C.mir_sdr_FsUpdateError
	HwError            = C.mir_sdr_HwError
	AliasingError      = C.mir_sdr_AliasingError
	AlreadyInitialized = C.mir_sdr_AlreadyInitialised
	NotInitialized     = C.mir_sdr_NotInitialised
	NotEnabled         = C.mir_sdr_NotEnabled
	HwVerError         = C.mir_sdr_HwVerError
	OutOfMemoryError   = C.mir_sdr_OutOfMemError
)

type BwMhz int

const (
	BW_Undefined BwMhz = C.mir_sdr_BW_Undefined
	BW_0_200     BwMhz = C.mir_sdr_BW_0_200
	BW_0_300     BwMhz = C.mir_sdr_BW_0_300
	BW_0_600     BwMhz = C.mir_sdr_BW_0_600
	BW_1_536     BwMhz = C.mir_sdr_BW_1_536
	BW_5_000     BwMhz = C.mir_sdr_BW_5_000
	BW_6_000     BwMhz = C.mir_sdr_BW_6_000
	BW_7_000     BwMhz = C.mir_sdr_BW_7_000
	BW_8_000     BwMhz = C.mir_sdr_BW_8_000
)

type IfKhz int

const (
	IfUndefined IfKhz = C.mir_sdr_IF_Undefined
	IfZero      IfKhz = C.mir_sdr_IF_Zero
	If_0_450    IfKhz = C.mir_sdr_IF_0_450
	If_1_620    IfKhz = C.mir_sdr_IF_1_620
	If_2_048    IfKhz = C.mir_sdr_IF_2_048
)

type TransferMode int

const (
	Isoch TransferMode = C.mir_sdr_ISOCH
	Bulk  TransferMode = C.mir_sdr_BULK
)

type ReasonForReinit int

const (
	ChangeNone   ReasonForReinit = C.mir_sdr_CHANGE_NONE
	ChangeGrFreq ReasonForReinit = C.mir_sdr_CHANGE_GR
	ChangeFsFreq ReasonForReinit = C.mir_sdr_CHANGE_FS_FREQ
	ChangeRfType ReasonForReinit = C.mir_sdr_CHANGE_RF_FREQ
	ChangeBwType ReasonForReinit = C.mir_sdr_CHANGE_BW_TYPE
	ChangeIfType ReasonForReinit = C.mir_sdr_CHANGE_IF_TYPE
	ChangeLoMode ReasonForReinit = C.mir_sdr_CHANGE_LO_MODE
	ChangeAmPort ReasonForReinit = C.mir_sdr_CHANGE_AM_PORT
)

type LoMode int

const (
	LoUndefined LoMode = C.mir_sdr_LO_Undefined
	LoAuto      LoMode = C.mir_sdr_LO_Auto
	Lo120Mhz    LoMode = C.mir_sdr_LO_120MHz
	Lo144Mhz    LoMode = C.mir_sdr_LO_144MHz
	Lo168Mhz    LoMode = C.mir_sdr_LO_168MHz
)

type Band int

const (
	BandAmLo  Band = C.mir_sdr_BAND_AM_LO
	BandAmMid Band = C.mir_sdr_BAND_AM_MID
	BandAmHi  Band = C.mir_sdr_BAND_AM_HI
	BandVhf   Band = C.mir_sdr_BAND_VHF
	Band3     Band = C.mir_sdr_BAND_3
	BandX     Band = C.mir_sdr_BAND_X
	Band4_5   Band = C.mir_sdr_BAND_4_5
	BandL     Band = C.mir_sdr_BAND_L
)

type SetGrMode int

const (
	UseSetGr        SetGrMode = C.mir_sdr_USE_SET_GR
	UseSetGrAltMode SetGrMode = C.mir_sdr_USE_SET_GR_ALT_MODE
	UseSetRspSetGr  SetGrMode = C.mir_sdr_USE_RSP_SET_GR
)

type RspIIBand int

const (
	RspIIBandUnknown RspIIBand = C.mir_sdr_RSPII_BAND_UNKNOWN
	RspIIBandAmLo    RspIIBand = C.mir_sdr_RSPII_BAND_AM_LO
	RspIIBanbdAmMid  RspIIBand = C.mir_sdr_RSPII_BAND_AM_MID
	RspIIBandAmHi    RspIIBand = C.mir_sdr_RSPII_BAND_AM_HI
	RspIIBandVhf     RspIIBand = C.mir_sdr_RSPII_BAND_VHF
	RspIIBand3       RspIIBand = C.mir_sdr_RSPII_BAND_3
	RspIIBandXLo     RspIIBand = C.mir_sdr_RSPII_BAND_X_LO
	RspIIBandXMid    RspIIBand = C.mir_sdr_RSPII_BAND_X_MID
	RspIIBandXHi     RspIIBand = C.mir_sdr_RSPII_BAND_X_HI
	RspIIBand4_5     RspIIBand = C.mir_sdr_RSPII_BAND_4_5
	RspIIBandL       RspIIBand = C.mir_sdr_RSPII_BAND_L
)

type RspIIAntennaSelect int

const (
	RspIIAntennaA = C.mir_sdr_RSPII_ANTENNA_A
	RspIIAntennaB = C.mir_sdr_RSPII_ANTENNA_B
)

type AgcControl int

const (
	AgcDisable AgcControl = C.mir_sdr_AGC_DISABLE
	Agc100Khz  AgcControl = C.mir_sdr_AGC_100HZ
	Agc50Khz   AgcControl = C.mir_sdr_AGC_50HZ
	Agc5Hz     AgcControl = C.mir_sdr_AGC_5HZ
)

type GainMessageId int

const (
	GainMessageStartId   GainMessageId = C.mir_sdr_GAIN_MESSAGE_START_ID
	AdcOverloadDetected  GainMessageId = C.mir_sdr_ADC_OVERLOAD_DETECTED
	AdcOverloadCorrected GainMessageId = C.mir_sdr_ADC_OVERLOAD_CORRECTED
)

type MinGainReduction int

const (
	ExtendedMinGr MinGainReduction = C.mir_sdr_EXTENDED_MIN_GR
	NormalMinGr   MinGainReduction = C.mir_sdr_NORMAL_MIN_GR
)

// ApiError is the the type of errors returned by the Go API
type ApiError struct {
	Code int // Error code returned by the C API.
}

// NewApiError creates and returns an API error with the specified code
func NewApiError(code C.mir_sdr_ErrT) ApiError {

	return ApiError{Code: int(code)}
}

// Error returns a string with the text of the error
func (e ApiError) Error() string {

	switch e.Code {
	case Success:
		return "Success"
	case Fail:
		return "Fail"
	case InvalidParam:
		return "InvalidParam"
	case OutOfRange:
		return "OutOfRange"
	case GainUpdateError:
		return "GainUpdateError"
	case RfUpdateError:
		return "RfUpdateError"
	case FsUpdateError:
		return "FsUppdateError"
	case HwError:
		return "HwError"
	case AliasingError:
		return "AliasingError"
	case AlreadyInitialized:
		return "AlreadyInitialized"
	case NotInitialized:
		return "NotInitialized"
	case NotEnabled:
		return "NotEnabled"
	case HwVerError:
		return "HwVerError"
	case OutOfMemoryError:
		return "OutOfMemoryError"
	}
	return "Invalid error code"
}

// SetRf adjusts the nominal tuner frequency maintained in the internal state of the API.
// Depending on the state of the abs parameter, the drfHz parameter is either applied as an offset
// from the internally stored state of the API or is used in an absolute manner to
// modify the internally stored state.
func SetRf(drfHz float64, abs bool, syncUpdate bool) error {

	cabs := C.int(0)
	if abs {
		cabs = 1
	}
	csync := C.int(0)
	if syncUpdate {
		csync = 1
	}
	err := C.mir_sdr_SetRf(C.double(drfHz), cabs, csync)
	if err == Success {
		return nil
	}
	return NewApiError(err)
}

// SetFs adjusts the nominal sample frequency maintained in the internal state of the API.
// Depending on the state of the abs parameter, the dfsHz parameter is either applied
// as an offset from the internally stored state of the API or is used in an absolute manner
// to modify the internal stored API state.
// This command will typically permit only small changes in sample frequency in the order of ±1000ppm.
// For large sample frequency changes a Uninit() and Init() at the new sample rate must be performed.
func SetFs(dfsHz float64, abs bool, syncUpdate bool, reCal bool) error {

	cabs := C.int(0)
	if abs {
		cabs = 1
	}
	csync := C.int(0)
	if syncUpdate {
		csync = 1
	}
	crec := C.int(0)
	if reCal {
		crec = 1
	}
	err := C.mir_sdr_SetFs(C.double(dfsHz), cabs, csync, crec)
	if err == Success {
		return nil
	}
	return NewApiError(err)
}

func SetGr(gRdb int, abs bool, syncUpdate bool) error {

	return nil
}

// ApiVersion returns the version of the library
func ApiVersion() (float32, error) {

	var version C.float
	err := C.mir_sdr_ApiVersion(&version)
	if err == Success {
		return float32(version), nil
	}
	return float32(version), NewApiError(err)
}

// DCoffsetIQimbalanceControl enables or disables the DC offset correction and
// IQ imbalance correction.
// By default both DC offset correction and IQ imbalance correction are enabled.
func DCoffsetIQimbalanceControl(DCenable bool, IQenable bool) error {

	dcen := C.uint(0)
	if DCenable {
		dcen = 1
	}
	iqen := C.uint(0)
	if IQenable {
		iqen = 1
	}
	err := C.mir_sdr_DCoffsetIQimbalanceControl(dcen, iqen)
	if err == Success {
		return nil
	}
	return NewApiError(err)
}

// DebugEnable enables or disables the emission of debug messages by the C API.
func DebugEnable(enable bool) error {

	cen := C.uint(0)
	if enable {
		cen = C.uint(1)
	}
	err := C.mir_sdr_DebugEnable(cen)
	if err == Success {
		return nil
	}
	return NewApiError(err)
}

// DecimateControl can be used to control whether decimation is enabled or not.
// Valid decimation factors are 2, 4, 8, 16, 32 or 64 only.
// If other values are specified then decimation will not be enabled.
// If wide band mode is selected, the decimation algorithm uses a sequence of
// half-band filters to achieve the required decimation,
// otherwise a box-filter is used which is much more efficient but may cause roll-off in the passband of
// the received signal depending on bandwidth.
// Otherwise, a simple block averaging is used to reduce the
// CPU load, but with increased in-band roll-off
func DecimateControl(enable bool, decimationFactor uint, wideBandSignal uint) error {

	en := C.uint(0)
	if enable {
		en = C.uint(1)
	}
	err := C.mir_sdr_DecimateControl(en, C.uint(decimationFactor), C.uint(wideBandSignal))
	if err == Success {
		return nil
	}
	return NewApiError(err)
}

//mir_sdr_ErrT mir_sdr_AgcControl(mir_sdr_AgcControlT enable, int setPoint_dBfs, int knee_dBfs,
//unsigned int decay_ms, int hang_ms, int syncUpdate,
//int lnaState)
